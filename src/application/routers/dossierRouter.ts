import express from 'express';
import { Request, Response } from 'express';
import { CreateDossier} from '../../domain/dossier/use-cases/dossier/createDossier';
import { GetDossier} from '../../domain/dossier/use-cases/dossier/getDossier';


export default function DossierRouter(
    createDossier : CreateDossier,
     getDossier :  GetDossier
     ){

    const router = express.Router();

    router.get('/', async(req: Request, res: Response) => {
        try {

            const dossier = getDossier.execute(req.body.id);
            res.send(dossier);

        } catch (error) {
            
            res.status(500).send({ message: "Error fetching data" });
            
        }
     });

     router.post('/', async(req: Request, res: Response) => {

        try {
            
            const idDossier = createDossier.execute(req.body);
            res.status(201).send(idDossier);

        } catch (error) {
            
            res.status(500).send({ message: "Error fetching data" });

        }
    });

    return router;
}