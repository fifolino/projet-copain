import { Dossier } from '../../model/dossier';

export interface CreateDossier{

    execute(dossier : Dossier): Promise<string>;

}