import { Dossier } from '../../model/dossier';

export interface GetDossier{

    execute(id : string): Promise<Dossier>;

}