import { Dossier } from '../model/dossier';

export interface DossierRepository {

    createDossier(dossier : Dossier) : Promise<boolean>;
    getDossier(id : string) : Promise<Dossier>;
}